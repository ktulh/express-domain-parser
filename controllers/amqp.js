const amqplib = require("amqplib");

module.exports = (onResultsReceived) => {
  let channel = null;
  const testAmqp = async () => {
    var open = amqplib.connect(process.env.AMQP_CONNECTION_STRING);

    // Publisher
    open
      .then(function (conn) {
        return conn.createChannel();
      })
      .then(function (ch) {
        return ch.assertQueue("tasks").then(function (ok) {
          channel = ch;
          // return ch.sendToQueue(q, Buffer.from("something to do"));
        });
      })
      .catch(console.warn);

    // Consumer
    open
      .then(function (conn) {
        return conn.createChannel();
      })
      .then(function (ch) {
        return ch.assertQueue("results").then(function (ok) {
          return ch.consume("results", async function (msg) {
            if (msg !== null) {
              const message = JSON.parse(msg.content.toString());
              await onResultsReceived(message);
              ch.ack(msg);
            }
          });
        });
      })
      .catch(console.warn);
  };

  testAmqp();

  return {
    sendTask: (body) =>
      channel.sendToQueue("tasks", Buffer.from(JSON.stringify(body))),
  };
};
