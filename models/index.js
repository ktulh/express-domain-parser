const { Sequelize, DataTypes } = require("sequelize");

const sequelize = new Sequelize(process.env.SQL_LINK);

const UrlList = sequelize.define(
  "UrlList",
  {
    name: {
      type: DataTypes.STRING(45),
      allowNull: true,
    },
  },
  {
    tableName: "url_list",
    underscored: true,
    updatedAt: false,
  }
);

const ListDomain = sequelize.define(
  "ListDomain",
  {
    url_list_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "UrlList",
        key: "id",
      },
    },
    domain_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Domain",
        key: "id",
      },
    },
  },
  {
    tableName: "list_domain",
    timestamps: false,
  }
);

const DomainContact = sequelize.define(
  "DomainContact",
  {
    domain_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Domain",
        key: "id",
      },
    },
    first_name: { type: DataTypes.STRING(45), allowNull: true },
    last_name: { type: DataTypes.STRING(45), allowNull: true },
    email: { type: DataTypes.STRING(100), allowNull: true },
    confidence: { type: DataTypes.TINYINT, allowNull: true },
  },
  {
    tableName: "domain_contact",
    timestamps: false,
  }
);

const Domain = sequelize.define(
  "Domain",
  {
    domain_name: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
  },
  {
    tableName: "domain",
    timestamps: false,
  }
);

UrlList.belongsToMany(Domain, {
  through: ListDomain,
  as: "domains",
  foreignKey: "url_list_id",
});

Domain.belongsToMany(UrlList, {
  through: ListDomain,
  as: "url_lists",
  foreignKey: "domain_id",
});

Domain.hasOne(DomainContact, { foreignKey: "domain_id", as: "contact" });

(async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
})();

module.exports = {
  sequelize,
  UrlList,
  ListDomain,
  DomainContact,
  Domain,
};
