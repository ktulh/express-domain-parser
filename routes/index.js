const express = require("express");
const { Op } = require("sequelize");
const { UrlList, ListDomain, DomainContact, Domain } = require("../models");
const amqp = require("../controllers/amqp");

const router = express.Router();
const { sendTask } = amqp((results) => {
  console.log(results);
});

router.get("/", async (req, res) => {
  // TODO: Сделать выдачу кол-ва контактов
  const results = await UrlList.findAll({
    include: [
      {
        model: Domain,
        as: "domains",
        include: [{ model: DomainContact, as: "contact" }],
      },
    ],
  });
  res.json(results);
});

router.post("/", async (req, res) => {
  const { name, domains } = req.body;

  const existsDomains = await Domain.findAll({
    where: {
      domain_name: {
        [Op.or]: domains,
      },
    },
  });

  const newDomainsList = domains.filter(
    (domain) => !existsDomains.find(({ domain_name }) => domain_name === domain)
  );
  const newDomains = await Domain.bulkCreate(
    newDomainsList.map((domain) => ({ domain_name: domain }))
  );

  const list = await UrlList.create({
    name,
    domains: domains.map((domain) => ({ name: domain })),
  });

  const listDomains = await ListDomain.bulkCreate(
    [...existsDomains, ...newDomains].map(({ id }) => ({
      url_list_id: list.id,
      domain_id: id,
    }))
  );

  sendTask(
    [...existsDomains, ...newDomains].map(({ id, domain_name: domain }) => ({
      id,
      domain,
    }))
  );

  res.json(list);
});

module.exports = router;
